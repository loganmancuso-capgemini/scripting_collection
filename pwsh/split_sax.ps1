###
# 'split.ps1'
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 25-02-2020--16:32:56
###

## Global
$FileFolder    = "C:\Users\lmancuso\Documents\JIRA\2734\iofiles\"
$FileName      = "original"
$FileExt       = "xml"
$LimitSize     = 2MB
$FilePath      = $FileFolder + $FileName + '.' + $FileExt
$ReadFile      = [io.file]::OpenRead($FilePath)
$Size          = new-object byte[] $LimitSize

# Start Logging
Clear-Host
# Start-Transcript -Path ".\split_sax.log"

# DO THE WORK #
###############


$RemainingData = 0 
$ID            = 0
Do 
 {
  $RemainingData = $ReadFile.Read($Size, 0 , $Size.Length)
  If($RemainingData -gt 0) {
    $NewChunk  = $FileFolder + 'part\' + "$ID" + '_' + $FileName + '.' + $FileExt
    $ChunkFile = [io.file]::OpenWrite($NewChunk)
    $ChunkFile.Write($Size, 0 , $RemainingData)
    $ChunkFile.Close()
  }
  $ID ++
 }While($RemainingData -gt 0)

$ReadFile.Close()

# End Logging
# Stop-Transcript