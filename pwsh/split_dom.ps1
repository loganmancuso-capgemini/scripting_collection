###
# 'split.ps1'
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 25-02-2020--16:32:56
###

## Global
param(
		$path,
		[int]$maxitems
	)
$file = Get-ChildItem $path
$xml_tag = "dbo.vw_OrganDonor" #the xml tag to split on
# Start Logging
Clear-Host
Start-Transcript -Path ".\split_dom.log"

# DO THE WORK #
###############

#Read file
$xml = [xml](Get-Content -Path $file.FullName | Out-String)
$organDonor = $xml.SelectSingleNode("//$xml_tag")

$parent = $organDonor.ParentNode

#Create copy-template
$copyxml = [xml]$xml.OuterXml
$copyproduct = $copyxml.SelectSingleNode("//$xml_tag")
$copyparent = $copyproduct.ParentNode
#Remove all but one product (to know where to insert new ones)
$copyparent.SelectNodes("$xml_tag") | Where-Object { $_ -ne $copyproduct } | ForEach-Object { $copyparent.RemoveChild($_) } > $null

$allproducts = @($parent.SelectNodes("$xml_tag"))
$totalproducts = $allproducts.Count

$fileid = 1
$i = 0

foreach ($p in $allproducts) {
	#IF beggining or full file, create new file
	if($i % $maxitems -eq 0) {
		#Create copy of file
		$newFile = [xml]($copyxml.OuterXml)
		#Get parentnode
		$newparent = $newFile.SelectSingleNode("//$xml_tag").ParentNode
		#Remove all products
		$newparent.SelectNodes("$xml_tag") | ForEach-Object { $newparent.RemoveChild($_) } > $null
	}

	#Copy productnode
	$cur = $newFile.ImportNode($p,$true)
	$newparent.AppendChild($cur) > $null

	#Add 1 to "items moved"
	$i++ 

	#IF Full file, save
	if(($i % $maxitems -eq 0) -or ($i -eq $totalproducts)) {
		$newfilename = $file.FullName.Replace( $file.Extension,"_0$fileid$($file.Extension)")
		$newFile.Save($newfilename)
		$fileid++
	}
}

# End Logging
Stop-Transcript