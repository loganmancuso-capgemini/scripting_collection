###
# 'split.ps1'
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 25-02-2020--16:32:56
###

## Global
# param(
# 		$path,
# 		[int]$maxitems
# 		$dest
# 	)
# Start Logging
Clear-Host
Start-Transcript -Path ".\merge.log"

# DO THE WORK #
###############
$files = @(Get-ChildItem C:\Users\lmancuso\Documents\JIRA\2735\iofiles\part\*.xml) 
# <?xml version=`"1.0`" encoding=`"utf-8`"?>
$finalXml = "<?xml version=`"1.0`" encoding=`"utf-8`"?><Donors>"
foreach ($file in $files) {
	Write-Output "Merging $file"
	$xml = Get-Content $file  | Select-Object -Skip 2 | Select-Object -SkipLast 1 | Out-String
	$finalXml += $xml.Trim()
}
$finalXml += "</Donors>"
([xml]$finalXml).Save("C:\Users\lmancuso\Documents\JIRA\2735\iofiles\output.xml")

# End Logging
Stop-Transcript
